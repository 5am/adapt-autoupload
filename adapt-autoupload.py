#!/usr/bin/env python3

import sys, os, time, json, re, getopt, subprocess, getpass
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.common.exceptions import *
from pykeepass import PyKeePass

def main():
    version = 'adapt-autoupload.py version 1.2.0'

    class bcolours:
        OK = '\033[92m'
        FAIL = '\033[91m'
        INFO = '\033[96m'
        ENDC = '\033[0m'

    path = '/home/user/scripts/autoupload/'
    plugin_dir = '/home/user/adapt/v4/release/'
    tick = '\u2713'
    cross = '\u2717'

    help = '''
Usage: python3 adapt-autoupload [OPTIONS]

    -h, --help                  print this help
    -l, --list                  list all plugins ready for release
    -p, --plugin [FILEPATH]     plugin to be uploaded (.zip)
    '''

    def upload_plugin():
        correct_passwd = False
        while correct_passwd == False:
            kp_passwd = getpass.getpass(prompt='KPXC password: ', stream=None)
            try:
                kp = PyKeePass(path + 'aats.kdbx', password=kp_passwd)
            except Exception:
                print(f'{bcolours.FAIL}[{cross}] Incorrect password!{bcolours.ENDC}')
            else:
                correct_passwd = True
        driver_opt = Options()
        driver_opt.headless = True
        driver = webdriver.Firefox(options=driver_opt, executable_path=r'/usr/local/bin/geckodriver')
        wait = WebDriverWait(driver, 300)

        with open(path + 'urls.json', encoding='utf-8') as sites_file:
            sites = json.loads(sites_file.read())
            print()
            for each in sites['sites']:
                print(each['name'] + ' ==> ' + each['url'])
            reply = str(input(f'\nConfirm {bcolours.INFO}{plugin}{bcolours.ENDC} is not bespoke/customer-specific. [Y/y] ')).lower().strip()
            if reply[:1] != 'y':
                print('Exiting...\n')
                sites_file.close()
                driver.quit()
                exit()
            for each in sites['sites']:
                try:
                    name = each['name']
                    url = each['url']
                    db_entry = kp.find_entries(title=name, first=True)
                    user = db_entry.username
                    passwd = db_entry.password
                    driver.get(url)
                    wait.until(EC.visibility_of_element_located((By.CLASS_NAME, 'login-input-username')))
                    user_input = driver.find_element_by_id('login-input-username')
                    user_input.clear()
                    user_input.send_keys(user)
                    passwd_input = driver.find_element_by_id('login-input-password')
                    passwd_input.clear()
                    passwd_input.send_keys(passwd)
                    driver.find_element_by_id('login-input-password').send_keys(Keys.RETURN)
                    wait.until(EC.visibility_of_element_located((By.CLASS_NAME, 'module-dashboard')))
                    driver.find_element_by_class_name('navigation-global-menu').click()
                    driver.find_element_by_class_name('fa-plug').click()
                    wait.until(EC.visibility_of_element_located((By.CLASS_NAME, 'pluginManagement-sidebar-upload')))
                    driver.find_element_by_class_name('pluginManagement-sidebar-upload').click()
                    wait.until(EC.visibility_of_element_located((By.CLASS_NAME, 'plugin-file')))
                    driver.find_element_by_class_name('plugin-file').send_keys(plugin_dir + plugin)
                    driver.find_element_by_class_name('pluginManagement-upload-sidebar-save-button').click()
                    wait.until(EC.visibility_of_element_located((By.CLASS_NAME, 'sweet-overlay')))
                    result = driver.find_element_by_tag_name('h2').get_attribute('innerHTML')
                    if result == 'Success':
                        msg = driver.find_element_by_tag_name('p').get_attribute('innerHTML')
                        print(f'{bcolours.OK}[{tick}] {name}: Plugin uploaded.{bcolours.ENDC}')
                    elif result == 'Plugin upload failed':
                        msg = driver.find_element_by_tag_name('pre').get_attribute('innerHTML')
                        if msg == 'Error: You already have this version of the plugin installed.':
                            print(f'{bcolours.OK}[{tick}] {name}: Plugin already installed.{bcolours.ENDC}')
                        else:
                            print(f'{bcolours.FAIL}[{cross}] {name}: {msg}{bcolours.ENDC}')
                    else:
                        print(f'{bcolours.FAIL}[{cross}] {name}: Something went wrong!{bcolours.ENDC}')
                except Exception:
                    print(f'{bcolours.FAIL}[{cross}] {name}: Something went wrong!{bcolours.ENDC}')
            print('\nExiting...\n')
            sites_file.close()
            driver.quit()
            exit()

    argv = sys.argv[1:]
    try:
        opts, args = getopt.getopt(argv, 'hlvp:', ['help', 'list',  'version', 'plugin='])
    except getopt.GetoptError as err:
        print(err)
        opts = []

    for opt, arg in opts:
        if opt in ['-h', '--help']:
            print(help)
            exit()
        elif opt in ['-v', '--version']:
            print(version)
            exit()
        elif opt in ['-l', '--list']:
            print()
            subprocess.run('ls ' + plugin_dir + '*.zip', shell=True, capture_output=False)
            print()
            exit()
        elif opt in ['-p', '--plugin']:
            plugin = arg
            valid = re.search('.zip$', plugin)
            release = subprocess.run('ls ' + plugin_dir + plugin, shell=True, capture_output=True).returncode == 0
            if valid == False or release == False:
                print('Incorrect file format!')
                exit()
            else:
                upload_plugin()
        else:
            print(help)
            exit()

if __name__ == '__main__':
    main()
