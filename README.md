# adapt-autoupload

A script to automate the process of uploading a single plugin to multiple Adapt authoring tool (AAT) instances. Running the script will ask for the password to a KeePassXC database (not included in repo), which holds the username and password for each AAT. The script assumes the user has plugin releases/masters ready to be uploaded from a specified local path.

Once the full path is given, the script will upload the file to each site it finds in urls.json (not included in repo), and return the relevant success or error message.

## Dependencies
- `python3`
- `python3-selenium`
- `python3-pykeepass`

## Using the script
```
$ python3 adapt-autoupload -h
        Usage: python3 adapt-autoupload.py [OPTIONS]

        -h, --help                print this help
        -l, --list                list all plugins ready for release
        -p, --plugin [FILEPATH]   plugin to be uploaded (.zip)
```
